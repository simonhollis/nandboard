PCBNEW-LibModule-V1  Tue 20 Aug 2013 09:04:56 BST
# encoding utf-8
Units mm
$INDEX
hole_4mm
$EndINDEX
$MODULE hole_4mm
Po 0 0 0 15 4EE52829 00000000 ~~
Li hole_4mm
Cd 4mm hole
Kw DEV
Sc 0
AR hole_4mm
Op 0 0 0
T0 0.508 -6.858 1.016 1.016 0 0.254 N I 21 N "4mm_hole"
T1 0.762 -8.382 1.016 1.016 0 0.254 N I 21 N "P***"
DC -0.02032 -0.02032 3.73888 -2.44094 0.381 21
$PAD
Sh "1" C 8.001 8.001 0 0 0
Dr 4.0005 0 0
At STD N 00F0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$SHAPE3D
Na "device/douille_4mm(red).wrl"
Sc 1.8 1.8 1.8
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE hole_4mm
$EndLIBRARY
