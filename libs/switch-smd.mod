PCBNEW-LibModule-V1  25/07/2014 17:38:19
# encoding utf-8
Units mm
$INDEX
switch-smd-ta-1143
$EndINDEX
$MODULE switch-smd-ta-1143
Po 0 0 0 15 53D287F8 00000000 ~~
Li switch-smd-ta-1143
Sc 0
AR /53D2839B
Op 0 0 0
T0 0 4.45 1 1 0 0.15 N V 21 N "SW1"
T1 0 -4.15 1 1 0 0.15 N V 21 N "Switch 1"
DC 0.05 0 -1 -1.25 0.15 21
DS -3.1 -3.1 3.1 -3.1 0.15 21
DS 3.1 -3.1 3.1 3.1 0.15 21
DS 3.1 3.1 -3.1 3.1 0.15 21
DS -3.1 3.1 -3.1 -3.1 0.15 21
$PAD
Sh "1" R 1.6 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "VCC"
Po -4.2 -2.25
$EndPAD
$PAD
Sh "4" R 1.6 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "/Switch_1_out"
Po 4.2 -2.25
$EndPAD
$PAD
Sh "2" R 1.6 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.2 2.25
$EndPAD
$PAD
Sh "3" R 1.6 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.2 2.25
$EndPAD
$EndMODULE switch-smd-ta-1143
$EndLIBRARY
