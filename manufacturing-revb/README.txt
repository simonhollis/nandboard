Design: nandboard
Revision: B

Build overview
--------------
Size 120x60mm
2 Layer PTH FR4 1oz
Total 43 plated holes
No NPTH
Hole size is finished size
Smallest hole 0.5mm
Min track and gap 0.2mm
Min track and gap (fill) 0.3mm
Solder mask front and back
Solder mask overlap 0.002in, minimum dam width 0.004in.
Silkscreen front
Assembly front only
Units in mm


Additional build information
----------------------------

Contact: Simon Hollis
Tel: 0117 3315238
Email: simon@cs.bris.ac.uk
Address: Merchant Venturers Building
Woodland Road
Bristol
United Kingdom
BS8 1UB

PCB Design Tool: KiCad

Description of files included in nandboard-FAB.zip:

./README.txt    This File

./gerbers/   This directory contains the following gerber files for the design.

nandboard-F_Cu.gtl	Top Copper Layer
nandboard-B_Cu.gbl	Bottom Copper Layer
nandboard-F_Mask.gts	Top Solder Mask
nandboard-B_Mask.gbs	Bottom Solder Mask
nandboard-F_Paste.gtp	Top Solder Paste
nandboard-F_SilkS.gbo	Top Silk Screen
nandboard-Edge_Cuts.gtr	PCB Edges (centre of drawn lines)
nandboard-Cmts_User.gbr	Comments Layer (for information only)


./drill

nandboard.drl	Excellon format drill file, plated holes - Decimal, absolute co-ordinates, mm. Hole size is finished size.
nandboard-drl_map.pho	Gerber format Drill File (plated through holes)


./bom

nandboard-BOM.xls	Excel format Bill of Materials


./xy/			XY Coponent poisition data (components are on front only)

nandboard-XY-Front.csv	XY Component Position Data (Front side) (Comma seperated format)
